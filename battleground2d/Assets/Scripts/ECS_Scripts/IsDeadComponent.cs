﻿using Unity.Entities;

public struct IsDeadComponent : IComponentData
{
    public bool isDead;
}